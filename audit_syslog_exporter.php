#!/usr/bin/env php
<?php 
/*
** Glaber
** Copyright (C) Glaber 2018-2023
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

declare(strict_types = 1);
$sfile = dirname(__FILE__).'/settings.php';
require_once dirname(__FILE__).'/CAuditExporter.php';

$config = include($sfile);

$exporter = new CAuditExporter();
$records = $exporter->ReadAuditLogFromApi($config);

if (count($records) > 0)
  send_remote_syslog($records, $config);


function send_remote_syslog(array $records, array $config) {
    $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    foreach($records as $num => $record) {
      $syslog_message = "<22>" . date('M d H:i:s ') . 'glaber'. ' ' . 'audit' . ': ' . json_encode($record);
      socket_sendto($sock, $syslog_message, strlen($syslog_message), 0, $config['syslog_host'], $config['syslog_port']);
    }
    socket_close($sock);
  }
  
 // send_remote_syslog("Test");
  # send_remote_syslog("Any log message");
  # send_remote_syslog("Something just happened", "other-component");
  # send_remote_syslog("Something just happened", "a-background-job-name", "whatever-app-name");


?>