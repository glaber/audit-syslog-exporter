<?php
/*
** Glaber
** Copyright (C) 2018-2023  Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

require_once(__DIR__."/ZabbixApi.php");
define ('AUDIT_DELAY_TIMEOUT', 5);

class CAuditExporter {

    private $time_till;
    private $time_from;
    private $api;
    private $ts_filename;

    public function __construct() {
        $this->time_till = time() - AUDIT_DELAY_TIMEOUT; 
    }

    public function ReadAuditLogFromApi($settings) {
  
        $this->api = new IntelliTrend\Zabbix\ZabbixApi();
        $this->ApiConnectByToken($settings['API_URL'], $settings['API_TOKEN']);

        $this->ReadPrevTimestamp($settings['audit_ts_filename']);
        
        $params = [
            'time_from' => $this->time_from,
            'time_till' => $this->time_till,
        ];
     
        if (isset($settings['audit_filter'])) {
            $params['filter'] = $settings['audit_filter'];
          }

        $audit_records = $this->ApiCall('auditlog.get', $params);
        
        $records = [];

        foreach($audit_records as $num => $record) {
            $rec = [];
            foreach ($settings['audit_fields'] as $num => $field) 
                $rec[$field] = $record[$field];
            
            $rec['resource']= $this->getResourceById($record['resourcetype']);
            $rec['action']= $this->getActionById($record['action']);
            $records[] = $rec;
        }
        $this->WriteTimestamp($settings['audit_ts_filename'], $this->time_till);

        return $records;
    }

    private function WriteTimestamp($filename, $time) {
        try {
          file_put_contents($filename, $time);
        } catch (\Throwable $th) {
            print("Cannot write current timestamp to '".$filename.", make sure it script has the permissions'\n");
            exit(-1);
        } 
    }

    private function ReadPrevTimestamp($filename) {
        $time_from = file_get_contents($filename);
        
        if (! $time_from) {
            print("Cannot read prev export time from '".$filename.'"\n');
            print("Will init with current time\n");
            $this->WriteTimestamp($filename,time());
            exit(-1);
        } 

        $this->time_from = $time_from + 1;
                
        if ($this->time_till <= $this->time_from) {
            print("Will not export this time, time_from > time_till, try in a few second");
            exit(-1);
        }
    }
        
    private function ApiConnectByToken($url, $token) {
        try {
            $this->api->loginToken($url, $token, ['timeout' => 300]);
            //this is similar to: $result = $zbx->call('apiinfo.version');
            $version = $this->api->getApiVersion();
            //print "Remote Zabbix API Version:$version\n";
        } catch (ZabbixApiException $e) {
            print "==== Zabbix API Exception ===\n";
            print 'Errorcode: '.$e->getCode()."\n";
            print 'ErrorMessage: '.$e->getMessage()."\n";
            exit;
        } catch (Exception $e) {
            print "==== Exception ===\n";
            print 'Errorcode: '.$e->getCode()."\n";
            print 'ErrorMessage: '.$e->getMessage()."\n";
            exit;
        }
    }
    
    private function ApiCall($method, $options = []) {
        try {
              $result = $this->api->call($method, $options);
            //  print("Retrieved method $method with options ". json_encode($options)."\n total ". count($result)." records \n");
              return $result;    
          } catch (ZabbixApiException $e) {
              print "==== Zabbix API Exception ===\n";
              print 'Errorcode: '.$e->getCode()."\n";
              print 'ErrorMessage: '.$e->getMessage()."\n";
              exit;
          } catch (Exception $e) {
              print "==== Exception ===\n";
              print 'Errorcode: '.$e->getCode()."\n";
              print 'ErrorMessage: '.$e->getMessage()."\n";
              exit;
          }
    }
    
    private function getResourceById($id) {
        $res = ["5"=>"Action","45"=>"API token","42"=>"Authentication","38"=>"Autoregistration",
                "51"=>"Connector","34"=>"Event correlation","33"=>"Dashboard","23"=>"Discovery rule",
                "6"=>"Graph","35"=>"Graph prototype","47"=>"High availability node","4"=>"Host",
                "14"=>"Host group","37"=>"Host prototype","41"=>"Housekeeping","32"=>"Icon mapping",
                "16"=>"Image","18"=>"Service","15"=>"Item","36"=>"Item prototype","29"=>"Macro",
                "27"=>"Maintenance","19"=>"Map","3"=>"Media type","39"=>"Module","26"=>"Proxy",
                "28"=>"Regular expression","22"=>"Web scenario","46"=>"Scheduled report","25"=>"Script",
                "40"=>"Settings","48"=>"SLA","30"=>"Template","43"=>"Template dashboard","13"=>"Trigger",
                "31"=>"Trigger prototype","50"=>"Template group","0"=>"User","49"=>"User directory",
                "11"=>"User group","44"=>"User role","17"=>"Value map"];
        if (isset($res[$id]))
            return $res[$id];
        return $id;
    }

    private function getActionById($id) {
        $act = ["8"=>"Login","9"=>"Failed login","4"=>"Logout","0"=>"Add","1"=>"Update",
                "2"=>"Delete","7"=>"Execute","10"=>"History clear","11"=>"Configuration refresh"];

        if (isset($act[$id]))
            return $act[$id];
        return $id;
    }
}

?>